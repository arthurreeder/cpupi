# cpupi Benchmark Test

**Generated in ChatGPT**

Just a simple Pi calculating test. Desinged for speed over accuacy so don't expect the Pi numbers to look right.

## Compile

```
gcc -o pi pi.c
```

## Running

```
./pi

Estimated value of pi: 3.1416494800
Elapsed time: 2.132284 seconds
```

You can also choose how many points you want it to calculate.

```
./pi 100   

Estimated value of pi: 3.1200000000
Elapsed time: 0.000053 seconds
```


Should work on most systems without issue.

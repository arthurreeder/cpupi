#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char* argv[]) {
  // Seed the random number generator with the current time
  srand(time(NULL));

  // Set the default number of points to generate
  long long num_points = 100000000;

  // Parse the number of points to generate from the command line (if provided)
  if (argc > 1) {
    num_points = strtoll(argv[1], NULL, 10);
  }

  // Record the starting time
  clock_t start = clock();

  // Generate and count the points inside the quarter circle
  long long num_inside = 0;
  for (long long i = 0; i < num_points; i++) {
    // Generate random coordinates in the range (-1, 1)
    double x = (double)rand() / RAND_MAX * 2 - 1;
    double y = (double)rand() / RAND_MAX * 2 - 1;

    // Check if the point lies inside the quarter circle
    if (x*x + y*y <= 1) {
      num_inside++;
    }
  }

  // Calculate and print the estimated value of pi
  double pi = 4.0 * num_inside / num_points;
  printf("Estimated value of pi: %.10f\n", pi);

  // Calculate and print the elapsed time
  clock_t end = clock();
  double elapsed_time = (double)(end - start) / CLOCKS_PER_SEC;
  printf("Elapsed time: %f seconds\n", elapsed_time);

  return 0;
}
